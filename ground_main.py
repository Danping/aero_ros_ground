#!/usr/bin/python2.7
import socket
import sys
import logging
import subprocess
from socket_utils import *

from main_dlg import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QDialog

def get_local_ip():
    # a little trick to get the IP address
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip


class GroundStation:
    def __init__(self):
        self._local_ip = get_local_ip()
        self._connected = False
        self._gstpipe = []

    def __del__(self):
        self.shutdown()

    def connect(self, host_address):
        self.shutdown()
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.settimeout(0.5)
        self._host = host_address
        self._port = 10000
        try:
            self._sock.connect((self._host, self._port))
            self._connected = True
            self._sock.settimeout(5)
        except socket.error:
            logging.error("cannot connect to '%s'!", self._host)
            return False
        return True

    def auto_connect(self):
        ip_num_list = self._local_ip.split(".")
        ip_num_list[3] = "1"

        guess_host_ip = ".".join(ip_num_list)
        if self.connect(guess_host_ip):
            logging.info("Succeed in connecting to '%s'!", guess_host_ip)
            return True

        if self.connect(self._local_ip):
            logging.info("Succeed in connecting to '%s'!", self._host)
            return True

        return False

    def send_cmd(self, cmd_str):
        logging.info(cmd_str)
        send_msg(self._sock, cmd_str)
        try:
            ret_str=recv_msg(self._sock)
            print ret_str
        except socket.error as serr:
            return "socket_error"
        return ret_str


    def camera_streaming_on(self):
        if not self._connected:
            return False
        #open the camera video sink
        self._gstpipe = subprocess.Popen(' gst-launch-1.0 -e -v udpsrc port=1234 ! application/x-rtp, encoding-name=JPEG,payload=26 ! rtpjpegdepay ! jpegdec !  autovideosink '.split())
        ret_str = self.send_cmd("streaming_on")
        if not self.check_ret(ret_str):
            return False
        if ret_str == "streaming_on OK!":
            return True
        return False

    def camera_streaming_off(self):
        if not self._connected:
            return False

        ret_str = self.send_cmd("streaming_off")
        if ret_str == "streaming_off OK!":
            return True
        return False

    def switch_camera(self, cam_str):
        if cam_str == "forward":
            self.send_cmd("switch_camera forward")
        elif cam_str == "downward":
            self.send_cmd("switch_camera downward")
        else:
            logging.error("Not supported!")

    def check_ret(self,ret_str):
        if ret_str == "socket_error":
            return False
        return True

    def get_topic_list(self):
        if not self._connected:
            return False

        ret_str = self.send_cmd("list_ros_topics")
        if not self.check_ret(ret_str):
            return False
        if ret_str == "list_ros_topics failed!":
            self._topics = []
            return False
        self._topics = ret_str.split("\n")
        return True

    def start_recording(self, topic_str):
        ret_str = self.send_cmd("record_on %s"%topic_str)
        if not self.check_ret(ret_str):
            return False
        if ret_str == "record_on OK!":
            return True
        return False

    def stop_recording(self):
        ret_str = self.send_cmd("record_off")
        if not self.check_ret(ret_str):
            return False
        if ret_str == "record_off OK!":
            return True
        return False

    def check_connection(self):
        ret_str = self.send_cmd("hb")
        if ret_str == "ae":
            return True
        return False

    def shutdown(self):
        if self._connected:
            logging.info("The connection to '%s' has been shutdown!", self._host)
            self._sock.shutdown(socket.SHUT_RDWR)
        if self._gstpipe:
            self._gstpipe.terminate()
        self._connected = False


class MainWindow(QDialog, Ui_dlg_main):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.bottom_center()
        self._group = QtWidgets.QButtonGroup(self)
        self._group.addButton(self.rb_forward)
        self._group.addButton(self.rb_downward)
        self.rb_forward.setChecked(True)
        self.disable_operation(True)
        self.rb_forward.toggled.connect(self.handle_camera_switch)
        self.rb_downward.toggled.connect(self.handle_camera_switch)

        self.setWindowTitle('Aero ROS ground station')
        self.show()
        self.pb_connect.clicked.connect(self.handle_connect)
        self.pb_record.clicked.connect(self.handle_record)
        #for recording the selected ros topics
        self._camera_model = "forward"
        self._items = []
        self._recording = False
        self._timer = QTimer(self)
        self._timer.timeout.connect(self.on_timer)

        self._gs = GroundStation()
        if self._gs.auto_connect():
            self.ed_server_host.setText(self._gs._host)
            self.ed_server_host.setEnabled(False)
            self.pb_connect.setChecked(True)
            self.on_connected()

    def __del__(self):
        self._gs.shutdown()

    def on_connected(self):
        self.pb_connect.setText("disconnect")
        self.disable_operation(False)
        self._gs.camera_streaming_on()
        self._gs.get_topic_list()
        self.update_all_list(self._gs._topics)
        self._timer.start(500)

    def on_disconnected(self):
        self.pb_connect.setText("connect")
        self.pb_record.setText("Record")
        self._gs.shutdown()
        self.list_all.reset()
        self._timer.stop()
        self._recording = False
        self.disable_operation(True)


    def on_timer(self):
        if self._gs._connected and not self._gs.check_connection():
            print "disconnected!"
            self.on_disconnected()


    def disable_operation(self, flag):
        self.ed_server_host.setDisabled(not flag)
        self.rb_downward.setDisabled(flag)
        self.rb_forward.setDisabled(flag)
        self.pb_record.setDisabled(flag)


    def update_all_list(self, items):
        self.list_all.reset()
        self._model = QStandardItemModel(self.list_all)
        for item in items:
            if item <> "":
                li = QStandardItem()
                li.setText(item)
                li.setEditable(False)
                li.setCheckable(True)
                self._model.appendRow(li)
        self.list_all.setModel(self._model)
        self._model.itemChanged.connect(self.set_items)

    def set_items(self,item):
        if item.checkState() == QtCore.Qt.Checked:
            self._items.append(item)
        if item.checkState() == QtCore.Qt.Unchecked:
            self._items.remove(item)

    def get_selected_rostopics(self):
        topic_list = []
        for item in self._items:
            topic_list.append(item.text())
        return ' '.join(topic_list)


    def handle_connect(self):
        if self._gs._connected:
            self._gs.shutdown()
            self.ed_server_host.setEnabled(True)
            self.on_disconnected()
        else:
            if self._gs.connect(self.get_host()):
                self.ed_server_host.setText(self._gs._host)
                self.ed_server_host.setEnabled(False)
                self.pb_connect.setChecked(True)
                self.on_connected()

    def handle_record(self):
        if self._gs._connected:
            if not self._recording:
                topic_str = self.get_selected_rostopics()
                if topic_str <> '':
                    if self._gs.start_recording(topic_str):
                        print "record starts!"
                        self._recording = True
                        self.pb_record.setText("Stop recording")
            else:
                if self._gs.stop_recording():
                    self._recording = False
                    self.pb_record.setText("Record")
                    print "record stops"

    def handle_camera_switch(self):
        if self._gs._connected:
            if self.rb_forward.isChecked():
                if self._camera_model <> "forward":
                    self._gs.switch_camera("forward")
                    self._camera_model = "forward"
            else:
                if self._camera_model <> "downward":
                    self._gs.switch_camera("downward")
                    self._camera_model = "downward"


    def get_host(self):
        return self.ed_server_host.text()

    def bottom_center(self):
        qr = self.frameGeometry()
        qr.moveCenter(QDesktopWidget().availableGeometry().center())
        by = QDesktopWidget().availableGeometry().bottom()-50
        qr.moveBottom(by)
        self.move(qr.topLeft())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
