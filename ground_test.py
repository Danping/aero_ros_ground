#!/usr/bin/python2.7
from ground_main import *

def test_connect():
    gs = GroundStation()
    if gs.connect("192.168.0.100"):
        print "connected!"
    if gs.connect("192.168.5.1"):
        print "connected!"
    else:
        print >> sys.stderr, "not able to connect"


def test_auto_connect():
    gs = GroundStation()
    gs.auto_connect()


def test_camera_streaming_on():
    gs = GroundStation()
    gs.auto_connect()
    if gs.camera_streaming_on():
        print 'streaming_on is OK!'

def test_camera_streaming_off():
    gs = GroundStation()
    gs.auto_connect()
    if gs.camera_streaming_off():
        print 'streaming_off is OK!'

def test_update_lists():
    app = QApplication(sys.argv)
    w = MainWindow()
    w._gs._connected = True
    w.disable_operation(False)
    w.update_all_list("test\na\nb\n".split("\n"))
    w.show()
    sys.exit(app.exec_())

def test_timer():
    app = QApplication(sys.argv)
    w = MainWindow()
    w._gs._connected = True
    w.disable_operation(False)
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG)
    #test_connect()
    #test_auto_connect()
    #test_camera_streaming_on()
    #test_camera_streaming_off()
    #test_update_lists()
    test_timer()

